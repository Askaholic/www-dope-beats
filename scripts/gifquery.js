var badgifs = [70, 80, 244, 253];

function init() {
	
		for(var c = 0; c < 5; c++) {
			loadGif();
		}
		
		$(window).scroll(function() {
			if($(document).height() - $(window).height() == $(window).scrollTop()) {
				loadGif();
			}
		});
}

function loadGif() {
	var num;
	do {
		num = (Math.floor(Math.random() * 318) + 1);
	} while(badgifs.indexOf(num) != -1);

	var url = "http://www.catgifpage.com/gifs/" + num + ".gif";
	var img = $("<div class='content'><img src='" + url + "' style='padding:30px 10px 0 15px' alt='404 error: " + num + "'></div><br/>");
	$("#container").append(img);
}