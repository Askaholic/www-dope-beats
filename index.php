<!DOCTYPE html>

<?php

$viewCountFile = fopen('views', 'r+');
$count = (int) fread($viewCountFile, filesize('views'));
fseek($viewCountFile, 0);
fwrite($viewCountFile, (++$count));
fclose($viewCountFile);
?>

<html lang="en">
    <head>
        <title>Dope Beats</title>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="images/favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link href='https://fonts.googleapis.com/css?family=Terminal+Dosis' rel='stylesheet' type='text/css' />
    </head>
    <body>
	<div class="header">
			<h1><a href="#">Dope Beats</a></h1>
	</div>
        <div class="container">
	<div class="centered-wrapper">
            <div class="content">
				<ul class="ca-menu">
					<li>
						<a href="#">
							<span class="ca-icon">F</span>
							<div class="ca-content">
								<h2 class="ca-main">Files</h2>
							</div>
						</a>
					</li>
					<li>
						<a href="gifs.php">
							<span class="ca-icon">I</span>
							<div class="ca-content">
								<h2 class="ca-main">Gifs</h2>
							</div>
						</a>
					</li>
					<li>
						<a href="a.html">
							<span class="ca-icon">K</span>
							<div class="ca-content">
								<h2 class="ca-main">Broken Link</h2>
							</div>
						</a>
					</li>
					<li>
						<a href="ctf/">
							<span class="ca-icon">`</span>
							<div class="ca-content">
								<h2 class="ca-main">CTF</h2>
							</div>
						</a>
					</li>
					<li>
						<a href="Battleship372/">
							<span class="ca-icon">]</span>
							<div class="ca-content">
								<h2 class="ca-main">Battleship</h2>
							</div>
						</a>
					</li>
				</ul>
            </div><!-- content
			<p> this is some text </p>-->
	</div>
	</div>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
    </body>
</html>
