<!DOCTYPE html>

<?php

$viewCountFile = fopen('viewsgif', 'r+');
$count = (int) fread($viewCountFile, filesize('viewsgif'));
fseek($viewCountFile, 0);
fwrite($viewCountFile, (++$count));
fclose($viewCountFile);
?>

<html lang="en">
	<head>
		<title>Dope Gifs</title>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="images/favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link href='https://fonts.googleapis.com/css?family=Terminal+Dosis' rel='stylesheet' type='text/css' />
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
		<script src="scripts/gifquery.js"></script>
		<script>$(document).ready(function(){init()});</script>
	</head>
	<body>
		<div class="header">
			<h1><a href="index.php">Dope Beats</a></h1>
		</div>
		<div class="container">
			<div class="centered-wrapper" id="container">
			</div>
		</div>
	</body>
</html>
