<?php
session_start();

define('SERVER','localhost');
define('USER', 'dope-beats.com');
define('PASS', 'DopeMelancholySQL42HYPERDRIVE*');
define('DB', 'dope_beats_db');
define('USER_TABLE', 'users');
define('FLAG_TABLE', 'flags');

$error = "";

$conn = new mysqli(SERVER, USER, PASS, DB);
$username = $_SESSION['username'];
$is_admin = $_SESSION['is_admin'];
$csrf_token = $_SESSION['csrf_token'];

if(empty($username)) {
    echo 'You must be logged in';
}
else if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($csrf_token) && $csrf_token == $_POST['token']) {
    if($_POST['action'] == 'add_c' && $is_admin) {
        $c_desc = fix_data($_POST['desc']);
        $c_key = fix_data($_POST['key']);
        $c_pnt = $_POST['points'];
        
        if(!preg_match('/^[0-9]*$/',$c_pnt)) {
            $error .= "Points must be entered as an integer";
        }
        else if(empty($c_desc) || empty($c_key)) {
            $error .= "Fields cannot be empty";
        }
        else {
            if($stmt = $conn->prepare("INSERT INTO " . FLAG_TABLE . " (description, flag, points) VALUES (?,?,?)")) {
                $stmt->bind_param("ssi", $c_desc, $c_key, $c_pnt);
                $stmt->execute();
                $error = "id:" . $conn->insert_id;
            }
            else if($conn->query("SELECT 1 FROM " . FLAG_TABLE . " LIMIT 1")) {
                        $error .= "Could not insert data" . $conn->error;
            }
            else if(($conn->query("CREATE TABLE " . FLAG_TABLE . " (
                    id SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                    description TEXT NOT NULL,
                    flag VARCHAR(20) NOT NULL,
                    points INT UNSIGNED DEFAULT 0)"))) {
                    if($stmt = $conn->prepare("INSERT INTO " . FLAG_TABLE . " (description, flag, points) VALUES (?,?,?)")) {
                        $stmt->bind_param("ssi", $c_desc, $c_key, $c_pnt);
                        $stmt->execute();
                        $error = "id:" . $conn->insert_id;
                    }
                }
                else {
                    $error .= "Could not create table: " . $conn->error . "/n";
                }
        }
        echo $error;
    }
    else if($_POST['action'] == 'rem_c' && $is_admin) {
        $sql = "DELETE FROM " . FLAG_TABLE . " WHERE id=" . $_POST['id'];
        if(!$conn->query($sql)){
            echo 'Could not remove challenge: ' . $conn->error . "/n";
        }
    }
    else if($_POST['action'] == 'sub_c') {
        if($stmt = $conn->prepare("SELECT flag FROM " . FLAG_TABLE . " WHERE id=? LIMIT 1")) {
            $c_id = $_POST['id'];
            if(!preg_match('/^[0-9]*$/', $c_id)) {
                echo "Id must be an integer";
            }
            $stmt->bind_param("i", $c_id);
            $stmt->bind_result($flag);
            $stmt->execute();
            $stmt->fetch();
            $stmt->close();
            if($flag == $_POST['key']) {
                if($stmt = $conn->prepare("SELECT challenges FROM " . USER_TABLE . " WHERE username=? LIMIT 1")) {
                    $stmt->bind_param("s", $username);
                    $stmt->bind_result($result);
                    $stmt->execute();
                    $stmt->fetch();
                    $stmt->close();
                    if($result == null) {
                        $result = "";
                    }
                    if(strpos($result, $c_id . "") === false) {
                        if($stmt = $conn->prepare("UPDATE " . USER_TABLE . " SET challenges=? WHERE username=?")) {
                            $c_list = $result . $c_id . " ";
                            $stmt->bind_param("ss", $c_list, $username);
                            $stmt->execute();
                            $stmt->close();
                            echo "true";
                        }
                        else {
                            echo "Could not update table: " . $conn->error;
                        }
                    }
                    else {
                        echo "true";
                    }
                }
                else {
                    echo "Could not get challenge list: " . $conn->error;
                }
            }
            else echo "false";
        }
    }
}
else {
	echo "false";
}

function fix_data($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
?>
