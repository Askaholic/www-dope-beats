<?php
session_start();

if(empty($_SESSION['username'])) {
    header('Location: login.php');
}
else if(isset($_GET['logout']) && $_GET['logout'] == 'true') {
    session_unset();
    session_destroy();
    header('Location: login.php');
}

define('SERVER','localhost');
define('USER', 'dope-beats.com');
define('PASS', 'DopeMelancholySQL42HYPERDRIVE*');
define('DB', 'dope_beats_db');
define('USER_TABLE', 'users');
define('FLAG_TABLE', 'flags');

$this_page = $_SERVER['PHP_SELF'];
$get_page = isset($_GET['page']) ? $_GET['page'] : "";
$is_admin = $_SESSION['is_admin'];
$username = $_SESSION['username'];
$error = "";


$csrf_token = generateToken(20);
$_SESSION['csrf_token'] = $csrf_token;

$conn = new mysqli(SERVER, USER, PASS, DB);

function generateToken($length) {
	$result = "";
	for($c = 0; $c < $length; $c++) {
		$result .= chr(rand(35, 127));
	}
	return $result;
}

?>

<html>
    <head>
        <title><?php
            if($get_page == "challenge") echo "Dope Challenges";
            else if($_GET['page'] == "users") echo "Dope Users";
            else echo "My Dope Profile";
        ?></title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <link rel="shortcut icon" href="/images/favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="/css/main.css" />
        <link rel="stylesheet" type="text/css" href="/css/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Terminal+Dosis" rel="stylesheet" type="text/css" />
		<input type="hidden" id="token" value="<?php echo $csrf_token ?>" />
    </head>
    <body>
        <div class="header">
            <h1><a href="https://www.dope-beats.com">Dope Beats</a></h1>
            <?php
            echo '<h4>Logged in as ' . $_SESSION['displayname'] . '<a href="profile.php?logout=true">Logout</a></h4>
	';
            ?>
	</div>
        <div class="container">
            <div class="centered-wrapper">
                <div class="content">
                    <ul class="ca-menu">
                        <?php 
                        if($get_page == 'profile') {
                            echo '<li style="border-color: #30b7ff;">
                            <a href="'. $this_page . '?page=profile">
                                <span class="ca-icon" style="color: #30b7ff;">S</span>
                                <div class="ca-content">
                                        <h2 class="ca-main" style="color: #30b7ff;">My Profile</h2>
                                </div>
                            </a>
                        </li>';
                        }
                        else {
                            echo '<li>
                            <a href="'. $this_page . '?page=profile">
                                <span class="ca-icon">S</span>
                                <div class="ca-content">
                                        <h2 class="ca-main">My Profile</h2>
                                </div>
                            </a>
                        </li>';
                        }
                        if($is_admin == true) {
                        if($get_page == 'users') {
                            echo '<li style="border-color: #30b7ff;">
                            <a href="'. $this_page . '?page=users">
                                <span class="ca-icon" style="color: #30b7ff;">U</span>
                                <div class="ca-content">
                                        <h2 class="ca-main" style="color: #30b7ff;">Users</h2>
                                </div>
                            </a>
                        </li>';
                        }
                        else {
                            echo '<li>
                            <a href="'. $this_page . '?page=users">
                                <span class="ca-icon">U</span>
                                <div class="ca-content">
                                        <h2 class="ca-main">Users</h2>
                                </div>
                            </a>
                        </li>';
                        }
                        }
                        if($get_page == 'challenge') {
                            echo '<li style="border-color: #30b7ff;">
                            <a href="'. $this_page . '?page=challenge">
                                <span class="ca-icon" style="color: #30b7ff;">q</span>
                                <div class="ca-content">
                                        <h2 class="ca-main" style="color: #30b7ff;">Challenges</h2>
                                </div>
                            </a>
                        </li>';
                        }
                        else {
                            echo '<li>
                            <a href="'. $this_page . '?page=challenge">
                                <span class="ca-icon">q</span>
                                <div class="ca-content">
                                        <h2 class="ca-main">Challenges</h2>
                                </div>
                            </a>
                        </li>';
                        }
                        ?>
                        <li>
                            <a href="scoreboard.php">
                                <span class="ca-icon">_</span>
                                <div class="ca-content">
                                        <h2 class="ca-main">Scoreboard</h2>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <?php
        if($get_page == 'profile') {
            echo '<div class="container">
                    <div class="centered-wrapper">
                        <h1>My Profile</h1>
                    </div>
                </div>';
        }
        else if($is_admin == true && $get_page == 'users') {
            $stmt = $conn->prepare("SELECT id, username, displayname, admin_priv FROM " . USER_TABLE);
            $stmt->bind_result($r_id, $r_uname, $r_dname, $r_priv);
            $stmt->execute();
            $script = '<script>
                $(document).ready(function() {';
            $form =  '<form method="post">';
            while($stmt->fetch()) {
                if($r_uname != $_SESSION['username']) {
                    $form .= '<div class="content" style="margin: 5px 5px 5px 5px;padding: 5px 5px 5px 5px">
                         <label style="font-size:18px">Modify<input type="checkbox" id="mod' . $r_id . '"/>
                         <label style="font-size:18px">Username<input disabled="disabled" type="text" value=' . $r_uname . ' id="uname' . $r_id .'"/></label>
                         <label style="font-size:18px">Name<input disabled="disabled" type="text" value=' . $r_dname . ' id="dname' . $r_id . '"/></label>
                         <label style="font-size:18px">Admin<input disabled="disabled" type="checkbox" ' . (($r_priv == 'Y') ? 'checked ' : '') . 'id="box' . $r_id . '"/></label>
                         <label style="font-size:18px">Remove<input disabled="disabled" type="checkbox" id="rm' . $r_id . '"/></label></div><br/>';
                    $script .= '$("#mod' . $r_id . '").click(function() {
                                if($("#mod' . $r_id . '").is(":checked")) {
                                    $("#uname' . $r_id . '").removeAttr("disabled");
                                    $("#dname' . $r_id . '").removeAttr("disabled");
                                    $("#box' . $r_id . '").removeAttr("disabled");
                                }
                                else {
                                    $("#uname' . $r_id . '").attr("disabled", "disabled");
                                    $("#dname' . $r_id . '").attr("disabled", "disabled");
                                    $("#box' . $r_id . '").attr("disabled", "disabled");
                                }
                            });';
                }
            }
            $script .= '});</script>';
            $form .= '<input type="submit" id="submit" value="Update"/></form>';
            echo '<div class="container">
                    <div class="centered-wrapper">
                        <h1>Users</h1>'
                    . $form . '
                    </div>
                </div>';
            echo $script;
        }
        else if($get_page == 'challenge') {
            $html = '<div class="container">
                    <div class="centered-wrapper">
                        <h1>Challenges</h1>
                    <div class="content" style="padding: 10px 10px 10px 10px;margin: 5px auto;max-width:50vw;text-align:left;" id="challenge-wrapper">';
            $script = '';
            $sql = '';
            
            if($stmt = $conn->prepare("SELECT challenges FROM " . USER_TABLE . " WHERE username=?")) {
                $stmt->bind_param("s", $username);
                $stmt->bind_result($challenges);
                $stmt->execute();
                $stmt->fetch();
                $stmt->close();
            }
            
            if($is_admin) {
                $sql = "SELECT id, description, flag, points FROM " . FLAG_TABLE;
            }
            else {
                $sql = "SELECT id, description, points FROM " . FLAG_TABLE;
            }
            $result = $conn->query($sql);
            if(!empty($result)) {
                //Display all challenges
                while($row = $result->fetch_assoc())
                {
                    if($is_admin) {
                        $html .= '<div style="margin:auto;margin-bottom: 10px;float:right;" id="rm' . $row['id'] . '"><a class="ca-icon" style="position:relative;color:#30b7ff;cursor:pointer;" id="remove" onclick="removeChallenge(' . $row['id'] . ')">\\</a></div>';
                    }
                    $html .= '<div class="sub-content" id="c' . $row['id'] . '">';
                    $html .= $row['id'] . ". ";
                    $html .= $row['description'] . "(Points: " . $row['points'] . ")" . '</br>';
                    if($is_admin) {
                        $html .= '<input type="text" id="key' . $row['id'] . '" value="' . $row['flag'] . '" disabled="disabled"/>';
                    }
                    else {
                        if(strpos($challenges, $row['id']) === false) {
                            $html .= '<input type="text" id="key' . $row['id'] . '" onkeypress="submitKeyPress(event, ' . $row['id'] . ')"/>
                                <a class="ca-icon" style="position:relative;color:#30b7ff;cursor:pointer;" onclick="submitKey(' . $row['id'] . ')" id="submit' . $row['id'] . '">/</a>';
                        }
                    }
                    $html .= '</div>';
                }
            }
            else {
                $html .= '
                        <h2>No challenges to show</h2>';
            }
            if($is_admin) {
                    $html .= '
                        <div hidden id="inputs">
                            <textarea rows="2" id="desc" style="width:100%;"></textarea></br>
                            <input type="text" id="key" value="KEY_"/><a class="ca-icon" style="position:relative;color:#30b7ff;cursor:pointer;padding:10px;" id="rand">*</a>
                            <input type="text" style="width:30px;" maxlength="25" id="points" value="1"></br>
                            <input type="button" onclick="addBttn()" value="Add" id="add"/>
                        </div>
                    <div style="margin:auto;margin-bottom: 10px;width: 30px;height: 30px;" id="cont"><a class="ca-icon" style="position:relative;color:#30b7ff;cursor:pointer;" id="bttn">+</a><a hidden href="#" class="ca-icon" style="position:relative;color:#30b7ff" id="cbttn">\\</a></div>';
                    $script .= '<script>
                $(document).ready(function() {
                    $("#bttn").click(function() {
                        $("#inputs").show();
                        $("#bttn").hide();
                        $("#cbttn").show();
                    });
                    $("#cbttn").click(function() {
                        $("#inputs").hide();
                        $("#bttn").show();
                        $("#cbttn").hide();
                    });
                    $("#rand").click(function() {
                        var t = "KEY_";
                        for(var c = 0; c < 16; c++) {
                            t += Math.floor(Math.random() * 10);
                        }
                        $("#key").val(t);
                    });
                });
                function addBttn() {
                        $.post("func.php", {
                            action : "add_c",
                            desc : $("#desc").val(),
                            key : $("#key").val(),
                            points: $("#points").val(),
							token: $("#token").val()
                        }, function(data, status) {
                            if(status == "success" && data.startsWith("id")) {
                                var id = data.substring(3, data.length);
                                var desc = $("#desc").val();
                                var key = $("#key").val();
                                var points = $("#points").val();
                                var html = \'';
                    $script .= '<div style="margin:auto;margin-bottom: 10px;float:right;" id="rm\' + id + \'"><a class="ca-icon" style="position:relative;color:#30b7ff;cursor:pointer;" id="remove" onclick="removeChallenge(\' + id + \')">\\\\</a></div>';
                    $script .= '<div class="sub-content" id="c\' + id + \'">';
                    $script .= "' + id + '. ' + desc + '(Points: ' + points + ')' + '" . '</br><input type="text" id="key\' + id + \'" ';
                    $script .= 'value="\' + key + \'" disabled="disabled"/>';
                    $script .= '</div>' . "';";
                    $script .= '
                                $("#inputs").hide();
                                $("#bttn").show();
                                $("#cbttn").hide();
                                $("#desc").val("");
                                $("#key").val("KEY_");
                                $("#cont").before(html);
                            }
                        });
                }
                function removeChallenge(c_id) {
                    $.post("func.php", {
                        action : "rem_c",
                        id : c_id,
						token: $("#token").val()
                    }, function(data, status) {
                        if(data == "") {
                            $("#rm" + c_id).remove();
                            $("#c" + c_id).remove();
                        }
                    });
                }
                </script>';
                }
                else {
                    $script .= '<script>
                        function submitKey(s_id) {
                            $.post("func.php", {
                                action: "sub_c",
                                id: s_id,
                                key : $("#key" + s_id).val(),
								token: $("#token").val()
                            }, function(data, success) {
                                if(data != "true" && data != "false") {
                                    alert("Data: " + data + "\nSuccess: " + success);
                                }
                                else if(data == "true") {
                                    $("#key" + s_id).remove();
                                    $("#submit" + s_id).remove();
                                }
                                else {
                                    $("#key" + s_id).val("");
                                }
                            });
                        }
                        function submitKeyPress(e, id) {
                            if(e.keyCode == 13) {
                                submitKey(id);
                            }
                        }
                    </script>';
                }
            $html .= '
                    </div>
                    </div>
                </div>';
            echo $html;
            echo $script;
        }
        ?>
        <div class="footer">
            
        </div>
        
    </body>
</html>
